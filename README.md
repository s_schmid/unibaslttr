# Letter class option file for letters from the Department of Mathematics and Computer Science at the University of Basel

## Information
This is an lco file for letters from the Department of Mathematics
and Computer Science at the University of Basel. The layout of the
letter is based on the offical .dotx file from the corporate design
page. (This includes logo, margins, footer, page number, ...)

## Maintainer
- Stefan Schmid
- <stef.schmid@unibas.ch>
- Personal website: https://math.unibas.ch/institut/personen/profil/profil/person/schmid-5/
- Website of package: https://bitbucket.org/s_schmid/unibaslttr

## Personal information for letters
Put all your personal information (name, e-mail, telephone number, ...)
in the 'personalinfo.lcs' file.
For the right division/Fachbereich set division variable in this file
to either math or cs. If no value is set, then the division is not
shown in the output.

## Language options
The language of the letter depends on the language which is loaded
with the babel package. If both, English and German, are loaded,
then the language will default to German.

